<?php

use Facebook\FacebookSession;

/**
 * @author Westermack Batanyita <westermack@safi.co.tz>
 */
session_start();
FacebookSession::setDefaultApplication('904740909537369', '7e804056e8e4e138d21dceb330caaaa8');

/**
 * Setting time-zone
 */
date_default_timezone_set('Africa/Dar_es_Salaam');

/**
 * Defining Directory Seperator
 */
define('DS', DIRECTORY_SEPARATOR);

/**
 * Defining the root of the application
 */
define('ROOT', __DIR__);

/**
 * Define selfies directory
 */
define('SELFIES_DIRECTORY', ROOT . DS . 'public' . DS . 'data' . DS . 'selfies');

/**
 * Defining image extension
 */
define('IMAGE_EXTENSION', 'jpg');
