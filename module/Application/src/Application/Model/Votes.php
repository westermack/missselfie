<?php

namespace Application\Model;

//Use Zend\ServiceManager\ServiceLocatorInterface;

class Votes {

    public $voter_id;
    public $votee_id;
    public $time;

    //protected $serviceLocator;
//    public function __construct(ServiceLocatorInterface $serviceLocator) {
//        $this->serviceLocator = $serviceLocator;
//    }

    function exchangeArray($data) {
        $this->voter_id = (isset($data['voter_id'])) ? $data['voter_id'] : null;
        $this->votee_id = (isset($data['votee_id'])) ? $data['votee_id'] : null;
        $this->time = (isset($data['time'])) ? $data['time'] : null;
    }

}
