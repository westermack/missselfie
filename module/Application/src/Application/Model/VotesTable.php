<?php

namespace Application\Model;

use Zend\Db\TableGateway\TableGateway,
    Zend\ServiceManager\ServiceLocatorInterface;

class VotesTable {

    protected $tableGateway;
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway, ServiceLocatorInterface $serviceLocator) {
        $this->tableGateway = $tableGateway;
        $this->serviceLocator = $serviceLocator;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveVote(Votes $vote) {
        $data = array(
            'voter_id' => $vote->voter_id,
            'votee_id' => $vote->votee_id
        );

        $row = $this->getVote($vote->voter_id, $vote->votee_id);

        if (!$row) {
            $this->tableGateway->insert($data);
        }
    }

    public function getVote($voter_id, $votee_id) {
        $voter_id = (int) $voter_id;
        $votee_id = (int) $votee_id;
        $rowset = $this->tableGateway->select(array('voter_id' => $voter_id, 'votee_id' => $votee_id));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getVotesByUser($user_id) {
        $user_id = (int) $user_id;
        return $this->tableGateway->select(array('voter_id' => $user_id));
    }

    public function getContestantVoteCount($contestantId) {
        $contestantId = (int) $contestantId;
        return number_format($this->tableGateway->select(array('votee_id' => $contestantId))->count());
    }

    public function deleteVote($voter_id, $votee_id) {
        $voter_id = (int) $voter_id;
        $votee_id = (int) $votee_id;
        $this->tableGateway->delete(array('voter_id' => $voter_id, 'votee_id' => $votee_id));
    }

}
