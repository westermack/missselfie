<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel,
    Facebook\FacebookRedirectLoginHelper;

class ContestantsController extends AbstractActionController {

    public function indexAction() {
        if ($this->getServiceLocator()->get('AuthService')->hasIdentity()) {
            $this->layout()->setTemplate('layout/user');
        }

        $contestantId = $this->params()->fromRoute('id');
        $contestantsTable = $this->getServiceLocator()->get('ContestantsTable');
        $contestant = $contestantsTable->getContestant($contestantId);
        if ($contestant) {
            $view = new ViewModel(array(
                'contestant' => $contestant,
                'profile_selfie' => $contestant->getProfileSelfie(),
                'other_selfies' => $contestant->getOtherSelfies()
            ));
            $helper = new FacebookRedirectLoginHelper($this->url()->fromRoute('missselfie') . 'user/login/facebook');
            $this->layout()->FBloginUrl = $helper->getLoginUrl();
            $this->layout()->contestant = $contestant;
            $view->setTemplate('user/index/index');

            return $view;
        } else {
            return $this->redirect()->toRoute('missselfie/contestants');
        }
    }

}
