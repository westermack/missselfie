<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Facebook\FacebookRedirectLoginHelper;

class IndexController extends AbstractActionController {

    public function indexAction() {
        if ($this->getServiceLocator()->get('AuthService')->hasIdentity()) {
            return $this->redirect()->toRoute('user');
        }
        $helper = new FacebookRedirectLoginHelper($this->url()->fromRoute('missselfie') . 'user/login/facebook');
        $this->layout()->selectedMenuItem = 'home';
        return new ViewModel(array(
            'FBloginUrl' => $helper->getLoginUrl()
        ));
    }

    public function aboutAction() {
        if ($this->getServiceLocator()->get('AuthService')->hasIdentity()) {
            $this->layout()->setTemplate('layout/user');
        }
        $helper = new FacebookRedirectLoginHelper($this->url()->fromRoute('missselfie') . 'user/login/facebook');
        $this->layout()->selectedMenuItem = 'about';
        $this->layout()->FBloginUrl = $helper->getLoginUrl();
        return new ViewModel();
    }

    public function howItWorksAction() {
        if ($this->getServiceLocator()->get('AuthService')->hasIdentity()) {
            $this->layout()->setTemplate('layout/user');
        }
        $helper = new FacebookRedirectLoginHelper($this->url()->fromRoute('missselfie') . 'user/login/facebook');
        $this->layout()->selectedMenuItem = 'how-works';
        $this->layout()->FBloginUrl = $helper->getLoginUrl();
        return new ViewModel();
    }

    public function privacyAction() {
        if ($this->getServiceLocator()->get('AuthService')->hasIdentity()) {
            $this->layout()->setTemplate('layout/user');
        }
        $helper = new FacebookRedirectLoginHelper($this->url()->fromRoute('missselfie') . 'user/login/facebook');
        $this->layout()->FBloginUrl = $helper->getLoginUrl();
        return new ViewModel();
    }

    public function contestantsAction() {
        if ($this->getServiceLocator()->get('AuthService')->hasIdentity()) {
            $this->layout()->setTemplate('layout/user');
        }
        $helper = new FacebookRedirectLoginHelper($this->url()->fromRoute('missselfie') . 'user/login/facebook');
        $this->layout()->selectedMenuItem = 'contestants';
        $this->layout()->FBloginUrl = $helper->getLoginUrl();

        $contestantsTable = $this->getServiceLocator()->get('ContestantsTable');
        return new ViewModel(array(
            'contestants' => $contestantsTable->fetchAll()
        ));
    }

    public function termsAction() {
        if ($this->getServiceLocator()->get('AuthService')->hasIdentity()) {
            $this->layout()->setTemplate('layout/user');
        }
        $helper = new FacebookRedirectLoginHelper($this->url()->fromRoute('missselfie') . 'user/login/facebook');
        $this->layout()->FBloginUrl = $helper->getLoginUrl();
        return new ViewModel();
    }

}
