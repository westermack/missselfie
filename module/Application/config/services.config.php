<?php

namespace Application;

use Zend\Db\ResultSet\ResultSet,
    Zend\Db\TableGateway\TableGateway,
    Application\Model\Votes,
    Application\Model\VotesTable;

return array(
    'abstract_factories' => array(),
    'aliases' => array(),
    'factories' => array(
        'VotesTable' => function($sm) {
    $tableGateway = $sm->get('VotesTableGateway');
    $table = new VotesTable($tableGateway, $sm);
    return $table;
},
        'VotesTableGateway' => function ($sm) {
    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    $resultSetPrototype = new ResultSet();
    $resultSetPrototype->setArrayObjectPrototype(new Votes());
    return new TableGateway('votes', $dbAdapter, null, $resultSetPrototype);
}
    ),
    'invokables' => array(),
    'services' => array(),
    'shared' => array(),
);
