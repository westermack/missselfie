<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            // This defines the hostname route which forms the base
            // of each "child" route
            'missselfie' => [
                'type' => 'Hostname',
                'options' => [
                    'route' => 'www.missselfie.co.tz',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    // This Segment route captures the requested controller
                    // and action from the URI and, through ModuleRouteListener,
                    // selects the correct controller class to
                    'single' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/:controller[/:id]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]*'
                            ],
                            'defaults' => [
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'index',
                                'action' => 'index',
                            ],
                        ],
                    ],
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/[:controller[/][:action][/:id]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'defaults' => [
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'index',
                                'action' => 'index',
                            ],
                        ],
                    ],
                    'about' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/about',
                            'defaults' => [
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'index',
                                'action' => 'about',
                            ],
                        ],
                    ],
                    'how-it-works' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/how-it-works',
                            'defaults' => [
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'index',
                                'action' => 'howItWorks',
                            ],
                        ],
                    ],
                    'contestants' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/contestants',
                            'defaults' => [
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'index',
                                'action' => 'contestants',
                            ],
                        ],
                    ],
                    'privacy' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/privacy',
                            'defaults' => [
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'index',
                                'action' => 'privacy',
                            ],
                        ],
                    ],
                    'terms' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/terms',
                            'defaults' => [
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'index',
                                'action' => 'terms',
                            ],
                        ],
                    ],
                ],
            ],
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Contestants' => 'Application\Controller\ContestantsController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
