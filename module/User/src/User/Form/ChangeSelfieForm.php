<?php

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class ChangeSelfieForm extends Form {

    public function __construct($name = null) {
        parent::__construct('change_selfie');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('enctype', 'multipart/formdata');

        $selfie = new Element\File('selfie');
        $selfie->setLabel('selfie');

        $submit = new Element('submit');
        $submit->setValue(gettext('Save selfie'));
        $submit->setAttributes(array(
            'type' => 'submit',
            'class' => 'btn btn-warning btn-lg'
        ));

        $this->add($selfie);
        $this->add($submit);
    }

}
