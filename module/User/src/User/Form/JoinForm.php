<?php

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class JoinForm extends Form {

    public function __construct($name = null) {
        parent::__construct('join');
        $this->setAttribute('method', 'POST');
        $this->setAttribute('enctype', 'multipart/formdata');

        $fullname = new Element('fullname');
        $fullname->setLabel(gettext('Full name'));
        $fullname->setAttributes(array(
            'required' => 'required',
            'type' => 'text',
            //'class' => 'form-control',
            'placeholder' => gettext('First name, as it appears on your birth certificate'),
            'maxlength' => '127'
        ));

        $birthdate = new Element\DateSelect('birthdate');
        $birthdate->setLabel(gettext('Date of birth, as it appears on your birth certificate'));
        $birthdate->setDayAttributes([
            'data-placeholder' => gettext('Day'),
            'required' => 'required',
            'style' => 'width: 30%; display:inline-block',
                //'class' => 'form-control',
        ]);
        $birthdate->setMonthAttributes([
            'data-placeholder' => gettext('Month'),
            'required' => 'required',
            'style' => 'width: 33%; display:inline-block',
                //'class' => 'form-control',
        ]);
        $birthdate->setYearAttributes([
            'data-placeholder' => gettext('Year'),
            'required' => 'required',
            'style' => 'width: 30%; display:inline-block',
                //'class' => 'form-control'
        ]);
        $birthdate->getDayElement()->setEmptyOption(gettext('Day'));
        $birthdate->getMonthElement()->setEmptyOption(gettext('Month'));
        $birthdate->getYearElement()->setEmptyOption(gettext('Year'));

        $physicaladdress = new Element('physicaladdress');
        $physicaladdress->setLabel(gettext('Physical address'));
        $physicaladdress->setAttributes(array(
            'required' => 'required',
            'type' => 'text',
            //'class' => 'form-control',
            'placeholder' => gettext('e.g Kinondoni B, Dar es Salaam')
        ));

        $bio = new Element('bio');
        $bio->setLabel(gettext('Short bio'));
        $bio->setAttributes(array(
            'required' => 'required',
            'type' => 'textarea',
            //'class' => 'form-control',
            'placeholder' => gettext('Tell us a bit about yourself, in a few words...'),
            'maxlength' => '300'
        ));

        $email = new Element('email');
        $email->setLabel(gettext('Valid email address'));
        $email->setAttributes(array(
            'required' => 'required',
            'type' => 'text',
            //'class' => 'form-control',
            'placeholder' => gettext('e.g mariamagdalena@gmail.com')
        ));

        $phone = new Element('phone');
        $phone->setLabel(gettext('Valid phone number'));
        $phone->setAttributes(array(
            'required' => 'required',
            //'class' => 'form-control',
            'type' => 'text',
            'placeholder' => gettext('e.g +2557886123456'),
            'maxlength' => '127'
        ));

        $selfie_1 = new Element\File('selfie_1');
        $selfie_1->setLabel('Selfie #1');

        $selfie_2 = new Element\File('selfie_2');
        $selfie_2->setLabel('Selfie #2');

        $selfie_3 = new Element\File('selfie_3');
        $selfie_3->setLabel('Selfie #3');

        $selfie_4 = new Element\File('selfie_4');
        $selfie_4->setLabel('Selfie #4');

        $selfie_5 = new Element\File('selfie_5');
        $selfie_5->setLabel('Selfie #5');

        $selfie_6 = new Element\File('selfie_6');
        $selfie_6->setLabel('Selfie #6');

        $submit = new Element('submit');
        $submit->setValue(gettext('Join contest'));
        $submit->setAttributes(array(
            'type' => 'submit',
            'class' => 'btn btn-warning btn-lg'
        ));

        $this->add($fullname);
        $this->add($birthdate);
        $this->add($bio);
        $this->add($physicaladdress);
        $this->add($email);
        $this->add($phone);
        $this->add($submit);
        $this->add($selfie_1);
        $this->add($selfie_2);
        $this->add($selfie_3);
        $this->add($selfie_4);
        $this->add($selfie_5);
        $this->add($selfie_6);
    }

}
