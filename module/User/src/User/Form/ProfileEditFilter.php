<?php

namespace User\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\InputFilter\ArrayInput;
use Zend\InputFilter\FileInput;

class ProfileEditFilter extends InputFilter {

    public function __construct() {
        $fullname = new Input('fullname');
        $fullname->getFilterChain()
                ->attachByName('StringTrim')
                ->attachByName('StripTags');
        $fullname->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Name cannot be empty')])
                ->attachByName('StringLength', ['encoding' => 'UTF-8', 'max' => 127]);

        $birthdate = new ArrayInput('birthdate');
        $birthdate->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Birthdate cannot be partial or empty')]);

        $bio = new Input('bio');
        $bio->getFilterChain()
                ->attachByName('StringTrim')
                ->attachByName('StripTags');
        $bio->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Bio cannot be empty')])
                ->attachByName('StringLength', ['encoding' => 'UTF-8', 'max' => 300]);

        $physicaladdress = new Input('physicaladdress');
        $physicaladdress->getFilterChain()
                ->attachByName('StringTrim')
                ->attachByName('StripTags');
        $physicaladdress->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Physical address cannot be empty')]);

        $email = new Input('email');
        $email->getFilterChain()
                ->attachByName('StringTrim')
                ->attachByName('StringToLower');
        $email->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Email cannot be empty')])
                ->attachByName('EmailAddress', ['message' => gettext('Please provide a valid email address')]);

        $phone = new Input('phone');
        $phone->getFilterChain()
                ->attachByName('StringTrim')
                ->attachByName('StripTags');
        $phone->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Phone number cannot be empty')]);

        $this->add($fullname);
        $this->add($birthdate);
        $this->add($bio);
        $this->add($physicaladdress);
        $this->add($email);
        $this->add($phone);
    }

}
