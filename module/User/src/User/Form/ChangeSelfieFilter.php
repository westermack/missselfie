<?php

namespace User\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\FileInput;

class ChangeSelfieFilter extends InputFilter {

    public function __construct() {
        $selfie = new FileInput('selfie');
        $selfie->setRequired(true);
        $selfie->getValidatorChain()
                ->addByName('FileMimeType', [
                    'mimeType' => ['image/gif', 'image/jpeg', 'image/png'],
                    'message' => 'Please provide a JPEG, PNG or GIF type of photo'
                ])->addByName('FileSize', ['max' => '6MB', 'message' => 'The photo you uploaded is too large. It cannot be larger than 6MB'])
                ->attachByName('fileimagesize', ['minWidth' => 400, 'minHeight' => 400, 'message' => 'The photo you uploaded is too small! It must be at least 400 pixels wide and 400 pixels tall']);

//        $selfie_1->getFilterChain()->attachByName(
//                'FileRenameUpload', [
//            'target' => getcwd() . '/data/tmp/selfie.jpg',
//            'randomize' => true,
//        ]);

        $this->add($selfie);
    }

}
