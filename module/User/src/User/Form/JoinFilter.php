<?php

namespace User\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\InputFilter\ArrayInput;
use Zend\InputFilter\FileInput;

class JoinFilter extends InputFilter {

    public function __construct() {
        $fullname = new Input('fullname');
        $fullname->getFilterChain()
                ->attachByName('StringTrim')
                ->attachByName('StripTags');
        $fullname->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Name cannot be empty')])
                ->attachByName('StringLength', ['encoding' => 'UTF-8', 'max' => 127]);

        $birthdate = new ArrayInput('birthdate');
        $birthdate->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Birthdate cannot be partial or empty')]);

        $bio = new Input('bio');
        $bio->getFilterChain()
                ->attachByName('StringTrim')
                ->attachByName('StripTags');
        $bio->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Bio cannot be empty')])
                ->attachByName('StringLength', ['encoding' => 'UTF-8', 'max' => 300]);

        $physicaladdress = new Input('physicaladdress');
        $physicaladdress->getFilterChain()
                ->attachByName('StringTrim')
                ->attachByName('StripTags');
        $physicaladdress->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Physical address cannot be empty')]);

        $email = new Input('email');
        $email->getFilterChain()
                ->attachByName('StringTrim')
                ->attachByName('StringToLower');
        $email->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Email cannot be empty')])
                ->attachByName('EmailAddress', ['message' => gettext('Please provide a valid email address')]);

        $phone = new Input('phone');
        $phone->getFilterChain()
                ->attachByName('StringTrim')
                ->attachByName('StripTags');
        $phone->getValidatorChain()
                ->attachByName('NotEmpty', ['message' => gettext('Phone number cannot be empty')]);

        $selfie_1 = new FileInput('selfie_1');
        $selfie_1->setRequired(true);
        $selfie_1->getValidatorChain()
                ->addByName('FileMimeType', [
                    'mimeType' => ['image/gif', 'image/jpeg', 'image/png'],
                    'message' => 'Please provide a JPEG, PNG or GIF type of photo'
                ])->addByName('FileSize', ['max' => '6MB', 'message' => 'The photo you uploaded is too large. It cannot be larger than 6MB'])
                ->attachByName('fileimagesize', ['minWidth' => 400, 'minHeight' => 400, 'message' => 'The photo you uploaded is too small! It must be at least 400 pixels wide and 400 pixels tall']);

//        $selfie_1->getFilterChain()->attachByName(
//                'FileRenameUpload', [
//            'target' => getcwd() . '/data/tmp/selfie_1.jpg',
//            'randomize' => true,
//        ]);

        $selfie_2 = new FileInput('selfie_2');
        $selfie_2->setRequired(true);
        $selfie_2->getValidatorChain()
                ->addByName('FileMimeType', [
                    'mimeType' => ['image/gif', 'image/jpeg', 'image/png'],
                    'message' => 'Please provide a JPEG, PNG or GIF type of photo'
                ])->addByName('FileSize', ['max' => '6MB', 'message' => 'The photo you uploaded is too large. It cannot be larger than 6MB'])
                ->attachByName('fileimagesize', ['minWidth' => 400, 'minHeight' => 400, 'message' => 'The photo you uploaded is too small! It must be at least 400 pixels wide and 400 pixels tall']);

//        $selfie_2->getFilterChain()->attachByName(
//                'FileRenameUpload', [
//            'target' => getcwd() . '/data/tmp/selfie_2.jpg',
//            'randomize' => true,
//        ]);

        $selfie_3 = new FileInput('selfie_3');
        $selfie_3->setRequired(true);
        $selfie_3->getValidatorChain()
                ->addByName('FileMimeType', [
                    'mimeType' => ['image/gif', 'image/jpeg', 'image/png'],
                    'message' => 'Please provide a JPEG, PNG or GIF type of photo'
                ])->addByName('FileSize', ['max' => '6MB', 'message' => 'The photo you uploaded is too large. It cannot be larger than 6MB'])
                ->attachByName('fileimagesize', ['minWidth' => 400, 'minHeight' => 400, 'message' => 'The photo you uploaded is too small! It must be at least 400 pixels wide and 400 pixels tall']);

//        $selfie_3->getFilterChain()->attachByName(
//                'FileRenameUpload', [
//            'target' => getcwd() . '/data/tmp/selfie_3.jpg',
//            'randomize' => true,
//        ]);

        $selfie_4 = new FileInput('selfie_4');
        $selfie_4->setRequired(true);
        $selfie_4->getValidatorChain()
                ->addByName('FileMimeType', [
                    'mimeType' => ['image/gif', 'image/jpeg', 'image/png'],
                    'message' => 'Please provide a JPEG, PNG or GIF type of photo'
                ])->addByName('FileSize', ['max' => '6MB', 'message' => 'The photo you uploaded is too large. It cannot be larger than 6MB'])
                ->attachByName('fileimagesize', ['minWidth' => 400, 'minHeight' => 400, 'message' => 'The photo you uploaded is too small! It must be at least 400 pixels wide and 400 pixels tall']);

//        $selfie_4->getFilterChain()->attachByName(
//                'FileRenameUpload', [
//            'target' => getcwd() . '/data/tmp/selfie_4.jpg',
//            'randomize' => true,
//        ]);

        $selfie_5 = new FileInput('selfie_5');
        $selfie_5->setRequired(true);
        $selfie_5->getValidatorChain()
                ->addByName('FileMimeType', [
                    'mimeType' => ['image/gif', 'image/jpeg', 'image/png'],
                    'message' => 'Please provide a JPEG, PNG or GIF type of photo'
                ])->addByName('FileSize', ['max' => '6MB', 'message' => 'The photo you uploaded is too large. It cannot be larger than 6MB'])
                ->attachByName('fileimagesize', ['minWidth' => 400, 'minHeight' => 400, 'message' => 'The photo you uploaded is too small! It must be at least 400 pixels wide and 400 pixels tall']);

//        $selfie_5->getFilterChain()->attachByName(
//                'FileRenameUpload', [
//            'target' => getcwd() . '/data/tmp/selfie_5.jpg',
//            'randomize' => true,
//        ]);

        $selfie_6 = new FileInput('selfie_6');
        $selfie_6->setRequired(true);
        $selfie_6->getValidatorChain()
                ->addByName('FileMimeType', [
                    'mimeType' => ['image/gif', 'image/jpeg', 'image/png'],
                    'message' => 'Please provide a JPEG, PNG or GIF type of photo'
                ])->addByName('FileSize', ['max' => '6MB', 'message' => 'The photo you uploaded is too large. It cannot be larger than 6MB'])
                ->attachByName('fileimagesize', ['minWidth' => 400, 'minHeight' => 400, 'message' => 'The photo you uploaded is too small! It must be at least 400 pixels wide and 400 pixels tall']);

//        $selfie_6->getFilterChain()->attachByName(
//                'FileRenameUpload', [
//            'target' => getcwd() . '/data/tmp/selfie_6.jpg',
//            'randomize' => true,
//        ]);

        $this->add($fullname);
        $this->add($birthdate);
        $this->add($bio);
        $this->add($physicaladdress);
        $this->add($email);
        $this->add($phone);
        $this->add($selfie_1);
        $this->add($selfie_2);
        $this->add($selfie_3);
        $this->add($selfie_4);
        $this->add($selfie_5);
        $this->add($selfie_6);
    }

}
