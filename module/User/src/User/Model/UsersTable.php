<?php

namespace User\Model;

use Zend\Db\TableGateway\TableGateway,
    Zend\ServiceManager\ServiceLocatorInterface;

class UsersTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway, ServiceLocatorInterface $serviceLocator) {
        $this->tableGateway = $tableGateway;
        $this->serviceLocator = $serviceLocator;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveUser(Users $user) {
        $data = array(
        );
        $id = (int) $user->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUser($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('User ID does not exist');
            }
        }
    }

    public function getUser($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getIsContestant($id) {
        $contestantsTable = $this->serviceLocator->get('ContestantsTable');
        return $contestantsTable->getContestant($id) ? true : false;
    }

    public function getLastUserId() {
        return $this->tableGateway->getLastInsertValue();
    }

    public function deleteUser($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

}
