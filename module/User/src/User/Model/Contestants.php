<?php

namespace User\Model;

Use Zend\ServiceManager\ServiceLocatorInterface;

class Contestants {

    public $user_id;
    public $fullname;
    public $bio;
    public $birthdate;
    public $physicaladdress;
    public $email;
    public $phone;
    public $time;
    protected $serviceLocator;

    public function __construct(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    function exchangeArray($data) {
        $this->user_id = (isset($data['user_id'])) ? $data['user_id'] : null;
        $this->fullname = (isset($data['fullname'])) ? $data['fullname'] : null;
        $this->bio = (isset($data['bio'])) ? $data['bio'] : null;
        $this->birthdate = (isset($data['birthdate'])) ? $data['birthdate'] : null;
        $this->physicaladdress = (isset($data['physicaladdress'])) ? $data['physicaladdress'] : null;
        $this->email = (isset($data['email'])) ? $data['email'] : null;
        $this->phone = (isset($data['phone'])) ? $data['phone'] : null;
        $this->time = (isset($data['time'])) ? $data['time'] : null;
    }

    public function getAge() {
        return floor((time() - strtotime($this->birthdate)) / (60 * 60 * 24 * 365));
    }

    public function getId() {
        return $this->user_id;
    }

    public function getName() {
        return $this->fullname;
    }

    public function getProfileSelfie() {
        return $this->serviceLocator->get('ContestantsTable')->getContestantProfileSelfie($this->user_id);
    }

    public function getOtherSelfies() {
        return $this->serviceLocator->get('ContestantsTable')->getContestantOtherSelfies($this->user_id);
    }

    public function getVoteCount() {
        return $this->serviceLocator->get('VotesTable')->getContestantVoteCount($this->user_id);
    }

}
