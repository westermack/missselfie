<?php

namespace User\Model;

use Zend\Db\TableGateway\TableGateway,
    Zend\ServiceManager\ServiceLocatorInterface;

class UsersOauthsTable {

    protected $tableGateway;
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway, ServiceLocatorInterface $serviceLocator) {
        $this->tableGateway = $tableGateway;
        $this->serviceLocator = $serviceLocator;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveUserOauth(UsersOauths $userOauth) {
        $data = array(
            'service' => $userOauth->service,
            'oauth_uid' => $userOauth->oauth_uid,
            'user_id' => $userOauth->user_id,
        );
        $this->tableGateway->insert($data);

//        $id = (int) $user->id;
//        if ($id == 0) {
//            $this->tableGateway->insert($data);
//        } else {
//            if ($this->getUser($id)) {
//                $this->tableGateway->update($data, array('id' => $id));
//            } else {
//                throw new \Exception('User ID does not exist');
//            }
//        }
    }

    public function getUserOauth($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getUserOauthByServiceId($service, $id) {
        $rowset = $this->tableGateway->select(array('service' => $service, 'oauth_uid' => $id));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $ service and $ id");
        }
        return $row;
    }

    public function getUserOauthByMissSelfieId($service, $id) {
        $rowset = $this->tableGateway->select(array('service' => $service, 'user_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $ service and $ id");
        }
        return $row;
    }

    public function getLoggedUser($service = 'facebook') {
        $authService = $this->serviceLocator->get('AuthService');
        if (!$authService->hasIdentity()) {
            return null;
        }
        $graphObject = $this->serviceLocator->get('FacebookGraphObject');
        $fbId = $graphObject->getProperty('id');
        $rowset = $this->tableGateway->select(array('service' => $service, 'oauth_uid' => $fbId));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $ service and $ id");
        }
        return $row;
    }

    public function getLoggedUserId($service = 'facebook') {
        $user = $this->getLoggedUser();
        if ($user) {
            return $user->user_id;
        } else {
            return null;
        }
    }

    public function deleteUserOauth($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

}
