<?php

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

class SelfiesTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveSelfie(Selfies $selfie) {
        $data = array(
            'id' => $selfie->id,
            'src' => $selfie->src,
            'owner_id' => $selfie->owner_id,
            'is_cover' => $selfie->is_cover
        );
        $id = (int) $selfie->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getSelfie($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Selfie ID does not exist');
            }
        }
    }

    public function getSelfie($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getLastSelfieId() {
        return $this->tableGateway->getLastInsertValue();
    }

    public function deleteSelfie($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

    public function getTableGateway() {
        return $this->tableGateway;
    }

}
