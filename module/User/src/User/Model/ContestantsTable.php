<?php

namespace User\Model;

use Zend\Db\TableGateway\TableGateway,
    Zend\ServiceManager\ServiceLocatorInterface;

class ContestantsTable {

    protected $tableGateway;
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway, ServiceLocatorInterface $serviceLocator) {
        $this->tableGateway = $tableGateway;
        $this->serviceLocator = $serviceLocator;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveContestant(Contestants $contestant) {
        $data = array(
            'user_id' => $contestant->user_id,
            'fullname' => $contestant->fullname,
            'bio' => $contestant->bio,
            'birthdate' => $contestant->birthdate,
            'physicaladdress' => $contestant->physicaladdress,
            'email' => $contestant->email,
            'phone' => $contestant->phone,
        );

        $row = $this->getContestant($contestant->user_id);

        if (!$row) {
            $this->tableGateway->insert($data);
        } else {
            $data['time'] = $row->time;
            $this->tableGateway->update($data, array('user_id' => $contestant->user_id));
        }
    }

    public function getContestant($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('user_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getContestantsById(array $ids) {
        return $this->tableGateway->select(array('user_id' => $ids));
    }

    public function getContestantProfileSelfie($id) {
        $id = (int) $id;
        $selfiesTable = $this->serviceLocator->get('SelfiesTable');
        $rowset = $selfiesTable->getTableGateway()->select(array('owner_id' => $id, 'is_cover' => 1));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $ service and $ id");
        }
        return $row;
    }

    public function getContestantOtherSelfies($id) {
        $id = (int) $id;
        $selfiesTable = $this->serviceLocator->get('SelfiesTable');
        $rowset = $selfiesTable->getTableGateway()->select(array('owner_id' => $id, 'is_cover' => 0));
        $selfies = [];
        foreach ($rowset as $selfie) {
            //$row = $rowset->current();
            if ($selfie) {
                $selfies[] = $selfie;
            }
        }

        return $selfies;
    }

    public function deleteContestant($id) {
        $this->tableGateway->delete(array('user_id' => $id));
    }

}
