<?php

namespace User\Model;

class Selfies {

    public $id;
    public $src;
    public $owner_id;
    public $is_cover;
    public $time;

    function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->src = (isset($data['src'])) ? $data['src'] : null;
        $this->owner_id = (isset($data['owner_id'])) ? $data['owner_id'] : null;
        $this->is_cover = (isset($data['is_cover'])) ? $data['is_cover'] : null;
        $this->time = (isset($data['time'])) ? $data['time'] : null;
    }

    public function getOwnerId() {
        return $this->owner_id;
    }

}
