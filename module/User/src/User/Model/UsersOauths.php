<?php

namespace User\Model;

Use Zend\ServiceManager\ServiceLocatorInterface;

class UsersOauths {

    public $service;
    public $oauth_uid;
    public $user_id;
    public $time;
    protected $serviceLocator;

    public function __construct(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    function exchangeArray($data) {
        $this->service = (isset($data['service'])) ? $data['service'] : 'facebook';
        $this->oauth_uid = (isset($data['oauth_uid'])) ? $data['oauth_uid'] : null;
        $this->user_id = (isset($data['user_id'])) ? $data['user_id'] : null;
        $this->time = (isset($data['time'])) ? $data['time'] : null;
    }

    public function getVotedContestants() {
        $votesTable = $this->serviceLocator->get('VotesTable');
        $votes = $votesTable->getVotesByUser($this->user_id);
        if ($votes->count() != 0) {
            $votee_ids = [];
            foreach ($votes as $vote) {
                $votee_ids[] = $vote->votee_id;
            }
            $contestantsTable = $this->serviceLocator->get('ContestantsTable');
            return $contestantsTable->getContestantsById($votee_ids);
        } else {
            return null;
        }
    }

}
