<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Facebook\FacebookRedirectLoginHelper,
    Facebook\FacebookRequestException,
    User\Model\Users,
    User\Model\UsersOauths,
    User\Model\Contestants,
    User\Model\Selfies,
    Application\Model\Votes,
    Imagine\Gd\Imagine,
    Imagine\Image\Box,
    Imagine\Image\ImageInterface;

class IndexController extends AbstractActionController {

    protected $service;
    protected $serviceId;

    public function indexAction() {
        $authService = $this->getServiceLocator()->get('AuthService');
        if (!$authService->hasIdentity()) {
            return $this->redirect()->toRoute('missselfie');
        }

        $usersTable = $this->getServiceLocator()->get('UsersTable');
        $usersOauthsTable = $this->getServiceLocator()->get('UsersOauthsTable');
        $user_id = $usersOauthsTable->getLoggedUserId();
        $contestantsTable = $this->getServiceLocator()->get('ContestantsTable');
        if ($usersTable->getIsContestant($user_id)) {
            $data = array(
                'contestant' => $contestantsTable->getContestant($user_id),
                'profile_selfie' => $contestantsTable->getContestantProfileSelfie($user_id),
                'other_selfies' => $contestantsTable->getContestantOtherSelfies($user_id)
            );
        } else {
            $data = array(
                'contestants' => $usersOauthsTable->getLoggedUser()->getVotedContestants()
            );
        }
        $this->layout()->selectedMenuItem = 'logged-home';
        return new ViewModel($data);
    }

    public function loginAction() {
        $helper = new FacebookRedirectLoginHelper($this->url()->fromRoute('missselfie') . 'user/login/facebook');
        try {
            $session = $helper->getSessionFromRedirect();
        } catch (FacebookRequestException $ex) {
            echo 1;
            echo '<br>';
            echo $ex->getMessage();
            exit;
// When Facebook returns an error
        } catch (\Exception $ex) {
            echo 2;
            echo '<br>';
            echo $ex->getMessage();
            exit;
// When validation fails or other local issues
        }
        if ($session) {
            $authService = $this->getServiceLocator()->get('AuthService');
            $authService->getStorage()->write($session);

            try {
                $graphObject = $this->getServiceLocator()->get('FacebookGraphObject');
                $this->service = $this->params()->fromRoute('id');
                $this->serviceId = $graphObject->getProperty('id');
                $usersOauthsTable = $this->getServiceLocator()->get('UsersOauthsTable');
                $user = $usersOauthsTable->getUserOauthByServiceId($this->service, $this->serviceId);
                if (!$user) {
                    $this->createUser(array());
                }
                return $this->redirect()->toRoute('user');
            } catch (FacebookRequestException $ex) {
                echo 3;
                echo '<br>';
                echo $ex->getMessage();
            } catch (\Exception $ex) {
                echo 4;
                echo '<br>';
                echo $ex->getMessage();
            }
        } else {
            return $this->redirect()->toRoute('missselfie');
        }
        return new ViewModel();
    }

    public function voteAction() {
        $authService = $this->getServiceLocator()->get('AuthService');
        $votee_id = $this->params()->fromRoute('id');
        if (!$authService->hasIdentity() || !$votee_id) {
            return $this->redirect()->toRoute('missselfie');
        }

        $usersTable = $this->getServiceLocator()->get('UsersTable');
        $usersOauthsTable = $this->getServiceLocator()->get('UsersOauthsTable');
        $user_id = $usersOauthsTable->getLoggedUserId();

        if ($usersTable->getIsContestant($user_id)) {
            return $this->redirect()->toRoute('user');
        }

        $votesTable = $this->getServiceLocator()->get('VotesTable');
        $vote = new Votes();
        $vote->exchangeArray(array(
            'voter_id' => $user_id,
            'votee_id' => $votee_id,
        ));
        $votesTable->saveVote($vote);

        return $this->redirect()->toRoute('user');
    }

    public function logoutAction() {
        $authService = $this->getServiceLocator()->get('AuthService');
        if (!$authService->hasIdentity()) {
            return $this->redirect()->toRoute('missselfie');
        }

        $authService->clearIdentity();

        return $this->redirect()->toRoute('missselfie');

        return new ViewModel();
    }

    public function editProfileAction() {
        $authService = $this->getServiceLocator()->get('AuthService');
        if (!$authService->hasIdentity()) {
            return $this->redirect()->toRoute('missselfie');
        }

        $usersTable = $this->getServiceLocator()->get('UsersTable');
        $usersOauthsTable = $this->getServiceLocator()->get('UsersOauthsTable');
        $user_id = $usersOauthsTable->getLoggedUserId();
        if (!$usersTable->getIsContestant($user_id)) {
            return $this->redirect()->toRoute('user');
        }

        $form = $this->getServiceLocator()->get('ProfileEditForm');
        $contestantsTable = $this->getServiceLocator()->get('ContestantsTable');
        $contestantsTable->getContestant($user_id);

        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost()->toArray());
            if (!$form->isValid()) {
                return new ViewModel(array(
                    'error' => true,
                    'form' => $form,
                ));
            }

            $data = $form->getData();

            $contestant = new Contestants($this->getServiceLocator());
            $contestant->exchangeArray(array(
                'user_id' => $this->getServiceLocator()->get('UsersOauthsTable')->getLoggedUserId(),
                'fullname' => $data['fullname'],
                'birthdate' => $data['birthdate'],
                'bio' => $data['bio'],
                'physicaladdress' => $data['physicaladdress'],
                'email' => $data['email'],
                'phone' => $data['phone'],
            ));

            $contestantsTable->saveContestant($contestant);
            return $this->redirect()->toRoute('user');
        }

        $contestant = $contestantsTable->getContestant($user_id);
        $data = array(
            'fullname' => $contestant->fullname,
            'birthdate' => $contestant->birthdate,
            'bio' => $contestant->bio,
            'physicaladdress' => $contestant->physicaladdress,
            'email' => $contestant->email,
            'phone' => $contestant->phone,
        );
        $form->setData($data);

        return new ViewModel(array(
            'form' => $form
        ));
    }

    public function changeSelfieAction() {
        $authService = $this->getServiceLocator()->get('AuthService');
        if (!$authService->hasIdentity()) {
            return $this->redirect()->toRoute('missselfie');
        }

        $usersTable = $this->getServiceLocator()->get('UsersTable');
        $usersOauthsTable = $this->getServiceLocator()->get('UsersOauthsTable');
        $selfieId = $this->params()->fromRoute('id');
        $loggedUserId = $usersOauthsTable->getLoggedUserId();

        $selfiesTable = $this->getServiceLocator()->get('SelfiesTable');
        $row = $selfiesTable->getSelfie($selfieId);
        $graphObject = $this->getServiceLocator()->get('FacebookGraphObject');

        if (!$selfieId || !$row || ((!$usersTable->getIsContestant($loggedUserId) || $loggedUserId != $row->getOwnerId()) && $graphObject->getProperty('id') != '903399166347269')) {
            return $this->redirect()->toRoute('user');
        }

        $form = $this->getServiceLocator()->get('ChangeSelfieForm');

        if ($this->getRequest()->isPost()) {
            $selfies = $this->getRequest()->getFiles()->toArray();

            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $selfies
            );

            $form->setData($data);
            if (!$form->isValid()) {
                return new ViewModel(array(
                    'error' => true,
                    'selfie_id' => $selfieId,
                    'form' => $form,
                ));
            }

            if ($loggedUserId != $row->getOwnerId()) {
                $user_FbId = $usersOauthsTable->getUserOauthByMissSelfieId('facebook', $row->getOwnerId())->oauth_uid;
            } else {
                $user_FbId = $graphObject->getProperty('id');
            }

            if (!is_dir(SELFIES_DIRECTORY)) {
                mkdir(SELFIES_DIRECTORY, 0766, true);
            }

            $selfie_img = $selfies['selfie'];

            $imagine = new Imagine();
            $imagine->open($selfie_img['tmp_name'])
                    ->thumbnail(new Box(720, 720), ImageInterface::THUMBNAIL_INSET)
                    ->save(SELFIES_DIRECTORY . DS . $selfieId . '_' . $user_FbId . '.' . IMAGE_EXTENSION);

            $imagine->open($selfie_img['tmp_name'])
                    ->thumbnail(new Box(400, 400), ImageInterface::THUMBNAIL_INSET)
                    ->save(SELFIES_DIRECTORY . DS . $selfieId . '_' . $user_FbId . '_n.' . IMAGE_EXTENSION);

            $imagine->open($selfie_img['tmp_name'])
                    ->thumbnail(new Box(400, 400), ImageInterface::THUMBNAIL_OUTBOUND)
                    ->save(SELFIES_DIRECTORY . DS . $selfieId . '_' . $user_FbId . '_ns.' . IMAGE_EXTENSION);

            return $this->redirect()->toRoute('user');
        }


        return new ViewModel(array(
            'selfie_id' => $selfieId,
            'form' => $form,
        ));
    }

    public function joinContestAction() {
        $authService = $this->getServiceLocator()->get('AuthService');
        if (!$authService->hasIdentity()) {
            return $this->redirect()->toRoute('missselfie');
        }

        $usersTable = $this->getServiceLocator()->get('UsersTable');
        $usersOauthsTable = $this->getServiceLocator()->get('UsersOauthsTable');
        $graphObject = $this->getServiceLocator()->get('FacebookGraphObject');
        if (($usersTable->getIsContestant($usersOauthsTable->getLoggedUserId()) || $graphObject->getProperty('gender') == 'male') && $graphObject->getProperty('id') != '903399166347269') {
            return $this->redirect()->toRoute('user');
        }

        $form = $this->getServiceLocator()->get('ContestantApplicationForm');

        if ($this->getRequest()->isPost()) {
            $selfies = $this->getRequest()->getFiles()->toArray();

            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $selfies
            );

            $form->setData($data);
            if (!$form->isValid()) {
                return new ViewModel(array(
                    'error' => true,
                    'form' => $form,
                ));
            }

            $this->createContestant($form->getData(), $selfies);
            return $this->redirect()->toRoute('user');
        }

        $this->layout()->selectedMenuItem = 'join-contest';
        return new ViewModel(array(
            'form' => $form
        ));
    }

//    public function settingsAction() {
//        $authService = $this->getServiceLocator()->get('AuthService');
//        if (!$authService->hasIdentity()) {
//            return $this->redirect()->toRoute('missselfie');
//        }
//    }

    protected function createUser(array $data) {
        $user = new Users();
        $user->exchangeArray($data);
        $usersTable = $this->getServiceLocator()->get('UsersTable');
        $usersTable->saveUser($user);

        $userId = $usersTable->getLastUserId();

        $userOauth = new UsersOauths($this->getServiceLocator());
        $userOauth->exchangeArray(array('service' => $this->service, 'oauth_uid' => $this->serviceId, 'user_id' => $userId));
        $usersOauthsTable = $this->getServiceLocator()->get('UsersOauthsTable');
        $usersOauthsTable->saveUserOauth($userOauth);
        return true;
    }

    protected function createContestant(array $data, array $selfies) {
        $contestant = new Contestants($this->getServiceLocator());
        $contestant->exchangeArray(array(
            'user_id' => $this->getServiceLocator()->get('UsersOauthsTable')->getLoggedUserId(),
            'fullname' => $data['fullname'],
            'birthdate' => $data['birthdate'],
            'bio' => $data['bio'],
            'physicaladdress' => $data['physicaladdress'],
            'email' => $data['email'],
            'phone' => $data['phone'],
        ));

        $contestantsTable = $this->getServiceLocator()->get('ContestantsTable');
        $contestantsTable->saveContestant($contestant);
        $graphObject = $this->getServiceLocator()->get('FacebookGraphObject');
        $user_FbId = $graphObject->getProperty('id');
        $selfiesTable = $this->getServiceLocator()->get('SelfiesTable');

        if (!is_dir(SELFIES_DIRECTORY)) {
            mkdir(SELFIES_DIRECTORY, 0766, true);
        }

        $selfie = new Selfies();
        $imagine = new Imagine();

        foreach ($selfies as $key => $selfie_img) {
            $selfie->exchangeArray(array(
                'src' => '',
                'owner_id' => $this->getServiceLocator()->get('UsersOauthsTable')->getLoggedUserId(),
                'is_cover' => $key === 'selfie_1' ? 1 : 0
            ));
            $selfiesTable->saveSelfie($selfie);

            $lastSelfieId = $selfiesTable->getLastSelfieId();

            $filename = $lastSelfieId . '_' . $user_FbId;

            $imagine->open($selfie_img['tmp_name'])
                    ->thumbnail(new Box(720, 720), ImageInterface::THUMBNAIL_INSET)
                    ->save(SELFIES_DIRECTORY . DS . $filename . '.' . IMAGE_EXTENSION);

            $imagine->open($selfie_img['tmp_name'])
                    ->thumbnail(new Box(400, 400), ImageInterface::THUMBNAIL_INSET)
                    ->save(SELFIES_DIRECTORY . DS . $filename . '_n.' . IMAGE_EXTENSION);

            $imagine->open($selfie_img['tmp_name'])
                    ->thumbnail(new Box(400, 400), ImageInterface::THUMBNAIL_OUTBOUND)
                    ->save(SELFIES_DIRECTORY . DS . $filename . '_ns.' . IMAGE_EXTENSION);

            $selfie->exchangeArray(array(
                'id' => $lastSelfieId,
                'src' => 'data/selfies/' . $filename,
                'owner_id' => $this->getServiceLocator()->get('UsersOauthsTable')->getLoggedUserId(),
                'is_cover' => $key === 'selfie_1' ? 1 : 0
            ));
            $selfiesTable->saveSelfie($selfie);
        }
    }

}
