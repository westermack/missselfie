<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Facebook\FacebookRedirectLoginHelper,
    Facebook\FacebookRequestException,
    Facebook\FacebookRequest;

class LoginController extends AbstractActionController {

    public function indexAction() {
        $helper = new FacebookRedirectLoginHelper($this->url()->fromRoute('missselfie') . 'user/login');
        try {
            $session = $helper->getSessionFromRedirect();
        } catch (FacebookRequestException $ex) {
            echo 1;
            echo '<br>';
            echo $ex->getMessage();
            exit;
            // When Facebook returns an error
        } catch (\Exception $ex) {
            echo 2;
            echo '<br>';
            echo $ex->getMessage();
            exit;
            // When validation fails or other local issues
        }
        if ($session) {
            try {
                $request = new FacebookRequest($session, 'GET', '/me');
                $response = $request->execute();
                $graphObject = $response->getGraphObject();
                print_r($graphObject->getProperty('id'));

                $userTable = $this->getServiceLocator()->get('UserTable');

                exit;
            } catch (FacebookRequestException $ex) {
                echo 3;
                echo '<br>';
                echo $ex->getMessage();
            } catch (\Exception $ex) {
                echo 4;
                echo '<br>';
                echo $ex->getMessage();
            }



            // Logged in
        } else {
            return $this->redirect()->toRoute('missselfie');
        }
        return new ViewModel();
    }

}
