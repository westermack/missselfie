<?php

namespace User\View\Helper;

use Zend\View\Helper\AbstractHelper,
    Facebook\FacebookRedirectLoginHelper;

class FacebookLoginHelper extends AbstractHelper {

    public function __invoke($redirect_url) {
        return new FacebookRedirectLoginHelper($redirect_url);
    }

}
