<?php

namespace User\View\Helper;

use Zend\View\Helper\AbstractHelper,
    Zend\ServiceManager\ServiceLocatorInterface;

class FacebookSession extends AbstractHelper {

    protected $serviceLocator;

    public function __invoke() {
        return $this->serviceLocator->get('FacebookSession');
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

}
