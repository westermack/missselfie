<?php

namespace User\View\Helper;

use Zend\View\Helper\AbstractHelper,
    Zend\ServiceManager\ServiceLocatorInterface;

class MissSelfieUserIdentity extends AbstractHelper {

    protected $serviceLocator;

    public function __invoke() {
        $authService = $this->serviceLocator->get('AuthService');
        if ($authService->hasIdentity()) {
            return $this;
        } else {
            return false;
        }
    }

    public function isContestant() {
        $contestantsTable = $this->serviceLocator->get('ContestantsTable');
        $usersOauthsTable = $this->serviceLocator->get('UsersOauthsTable');
        return $contestantsTable->getContestant($usersOauthsTable->getLoggedUserId()) ? true : false;
    }

    public function isContestantProfileOwner($profile_contestant_id) {
        return $this->serviceLocator->get('UsersOauthsTable')->getLoggedUserId() === $profile_contestant_id;
    }

    public function hasVotedForContestant($contestantId) {
        $votesTable = $this->serviceLocator->get('VotesTable');
        $usersOauthsTable = $this->serviceLocator->get('UsersOauthsTable');
        return $votesTable->getVote($usersOauthsTable->getLoggedUserId(), $contestantId);
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

}
