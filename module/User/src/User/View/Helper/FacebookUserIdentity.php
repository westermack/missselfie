<?php

namespace User\View\Helper;

use Zend\View\Helper\AbstractHelper,
    Zend\ServiceManager\ServiceLocatorInterface;

class FacebookUserIdentity extends AbstractHelper {

    protected $serviceLocator;

    public function __invoke() {
        $authService = $this->serviceLocator->get('AuthService');
        if ($authService->hasIdentity()) {
            return $this->serviceLocator->get('FacebookGraphObject');
        } else {
            return false;
        }
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

}
