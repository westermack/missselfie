<?php

namespace User;

use Zend\Db\ResultSet\ResultSet,
    Zend\Db\TableGateway\TableGateway,
    User\Model\Users,
    User\Model\UsersTable,
    User\Model\UsersOauths,
    User\Model\UsersOauthsTable,
    User\Model\Contestants,
    User\Model\ContestantsTable,
    User\Model\Selfies,
    User\Model\SelfiesTable,
    Facebook\FacebookSession,
    Facebook\FacebookRequest;

return array(
    'abstract_factories' => array(),
    'aliases' => array(),
    'factories' => array(
        'UsersTable' => function($sm) {
    $tableGateway = $sm->get('UsersTableGateway');
    $table = new UsersTable($tableGateway, $sm);
    return $table;
},
        'UsersTableGateway' => function ($sm) {
    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    $resultSetPrototype = new ResultSet();
    $resultSetPrototype->setArrayObjectPrototype(new Users());
    return new TableGateway('users', $dbAdapter, null, $resultSetPrototype);
},
        'UsersOauthsTable' => function($sm) {
    $tableGateway = $sm->get('UsersOauthsTableGateway');
    $table = new UsersOauthsTable($tableGateway, $sm);
    return $table;
},
        'UsersOauthsTableGateway' => function ($sm) {
    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    $resultSetPrototype = new ResultSet();
    $resultSetPrototype->setArrayObjectPrototype(new UsersOauths($sm));
    return new TableGateway('users_oauths', $dbAdapter, null, $resultSetPrototype);
},
        'ContestantsTable' => function($sm) {
    $tableGateway = $sm->get('ContestantsTableGateway');
    $table = new ContestantsTable($tableGateway, $sm);
    return $table;
},
        'ContestantsTableGateway' => function ($sm) {
    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    $resultSetPrototype = new ResultSet();
    $resultSetPrototype->setArrayObjectPrototype(new Contestants($sm));
    return new TableGateway('contestants', $dbAdapter, null, $resultSetPrototype);
},
        'SelfiesTable' => function($sm) {
    $tableGateway = $sm->get('SelfiesTableGateway');
    $table = new SelfiesTable($tableGateway);
    return $table;
},
        'SelfiesTableGateway' => function ($sm) {
    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    $resultSetPrototype = new ResultSet();
    $resultSetPrototype->setArrayObjectPrototype(new Selfies());
    return new TableGateway('selfies', $dbAdapter, null, $resultSetPrototype);
},
        'FacebookSession' => function ($sm) {
    $authService = $sm->get('AuthService');
    $session = $authService->getIdentity();
    if ($session) {
        try {
            $session->validate();
        } catch (FacebookRequestException $ex) {
            // Session not valid, Graph API returned an exception with the reason.
            //echo $ex->getMessage();
            $session = FacebookSession::newAppSession();
        } catch (\Exception $ex) {
            // Graph API returned info, but it may mismatch the current app or have expired.
            echo $ex->getMessage();
            exit;
        }
    } else {
        $session = FacebookSession::newAppSession();
    }
    return $session;
},
        'FacebookGraphObject' => function ($sm) {
    $session = $sm->get('FacebookSession');
    $request = new FacebookRequest($session, 'GET', '/me');
    $response = $request->execute();
    return $response->getGraphObject();
},
        // FORMS
        'ContestantApplicationForm' => function ($sm) {
    $form = new \User\Form\JoinForm();
    $form->setInputFilter($sm->get('ContestantApplicationFilter'));
    return $form;
},
        'ProfileEditForm' => function ($sm) {
    $form = new \User\Form\JoinForm();
    $form->setInputFilter($sm->get('ProfileEditFilter'));
    return $form;
},
        'ChangeSelfieForm' => function ($sm) {
    $form = new \User\Form\ChangeSelfieForm();
    $form->setInputFilter($sm->get('ChangeSelfieFilter'));
    return $form;
},
        // FORM FILTERS
        'ContestantApplicationFilter' => function ($sm) {
    return new \User\Form\JoinFilter();
},
        'ProfileEditFilter' => function ($sm) {
    return new \User\Form\ProfileEditFilter();
},
        'ChangeSelfieFilter' => function ($sm) {
    return new \User\Form\ChangeSelfieFilter();
}
    ),
    'invokables' => array(),
    'services' => array(),
    'shared' => array(),
);
