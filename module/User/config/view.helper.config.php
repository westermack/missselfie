<?php

namespace User;

return array(
    'factories' => array(
        'facebookUserIdentity' => function ($sm) {
    $serviceLocator = $sm->getServiceLocator();
    $viewHelper = new View\Helper\FacebookUserIdentity();
    $viewHelper->setServiceLocator($serviceLocator);
    return $viewHelper;
},
        'missSelfieUserIdentity' => function ($sm) {
    $serviceLocator = $sm->getServiceLocator();
    $viewHelper = new View\Helper\MissSelfieUserIdentity();
    $viewHelper->setServiceLocator($serviceLocator);
    return $viewHelper;
},
        'facebookLoginHelper' => function ($sm) {
    $viewHelper = new View\Helper\FacebookLoginHelper();
    return $viewHelper;
},
        'facebookSession' => function ($sm) {
    $serviceLocator = $sm->getServiceLocator();
    $viewHelper = new View\Helper\FacebookSession();
    $viewHelper->setServiceLocator($serviceLocator);
    return $viewHelper;
}
    ),
);
