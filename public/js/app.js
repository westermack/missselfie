$(function() {
    if (window.CoreStyle) {
        CoreStyle.g.paperInput.labelColor = '#FFFFFF';
        CoreStyle.g.paperInput.focusedColor = "#FFFFFF";
    }

    if ($('#home-hero .slider').size() !== 0) {
        $('#home-hero .slider').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            dots: true,
            arrows: false
        });
    }

    $(document).on('click', '.fb-share', function(e) {
        e.preventDefault();
        var thisElem = $(this);
        FB.ui(
                {
                    method: 'share',
                    href: thisElem.attr('data-share-url')
                }, function(response) {
        });
    });

    $(document).on('click', '.unlogged-voter', function(e) {
        e.preventDefault();
        $('#unlogged-voter-overlay').get(0).toggle();
    });

    $('[rel=selfies-gallery]').fancybox({
        //openEffect: 'none',
        //closeEffect: 'none',
        nextClick: true,
        preload: 6
    });

//    $(document).on('click', '.fb-login', function() {
//        FB.login(function(response) {
//            console.log('statusChangeCallback');
//            console.log(response);
//            // The response object is returned with a status field that lets the
//            // app know the current login status of the person.
//            // Full docs on the response object can be found in the documentation
//            // for FB.getLoginStatus().
//            if (response.status === 'connected') {
//                // Logged into your app and Facebook.
//                testAPI();
//            } else if (response.status === 'not_authorized') {
//                console.log('Please log into this app.');
//                // The person is logged into Facebook, but not your app.
//
//                //document.getElementById('status').innerHTML = 'Please log ' +
//                //        'into this app.';
//            } else {
//                console.log('Please log into Facebook.');
//                // The person is not logged into Facebook, so we're not sure if
//                // they are logged into this app or not.
//
//                //document.getElementById('status').innerHTML = 'Please log ' +
//                //        'into Facebook.';
//            }
//        }, {scope: 'public_profile,email'});
//    });


});