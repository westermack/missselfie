<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=missselfie;host=localhost',
        'username' => 'root',
        'password' => '1234.5678',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
//    'db' => array(
//        'driver' => 'Pdo',
//        'dsn' => 'mysql:dbname=missselfie;host=localhost',
//        'username' => 'marketpl_kerile',
//        'password' => 'good2011friday',
//        'driver_options' => array(
//            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
//        ),
//    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'AuthService' => function ($sm) {
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        $dbTableAuthAdapter = new Zend\Authentication\Adapter\DbTable($dbAdapter, 'users', 'id');
        $authService = new Zend\Authentication\AuthenticationService();
        $authService->setAdapter($dbTableAuthAdapter);
        return $authService;
    }
        ),
    ),
);
